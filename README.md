# README #

Catch of the Day is the real-time app built while following the tutorials at https://reactforbeginners.com/. It is modelled as a trendy seafood market where price and quantity available are variable and can change at a moments notice. The course instructs how to build a menu, an order form, and an inventory management area where authorized users can immediately update product details.  

### What is this repository for? ###
* Just persisting my work at the end of each chapter in the event I miss something and need to revert.
* Markup largely mimics the instructor's code except for a few captions and use of interpolation over concatenation.

### How do I get set up? ###
Technically this should just be read only; but install [NodeJS](http://nodejs.org), run `npm install` then `gulp`. Be sure to change the Firebase URL to your own.

### How to get started with the online demo ###
Watch this: http://recordit.co/IkBt2oMxFy