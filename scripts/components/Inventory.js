/*
  Inventory
  <Inventory/>
*/

import React from 'react';
import AddFishForm from './AddFishForm';
import autobind from 'autobind-decorator';
import Firebase from 'firebase';
const ref = new Firebase('https://sweltering-heat-8829.firebaseio.com/');

@autobind
class Inventory extends React.Component {

  constructor() {
    super();

    this.state = {
      uid: ''
    }
  }

  authenticate(provider) {
    ref.authWithOAuthPopup(provider, this.authHandler);
  }

  logout() {
    ref.unauth();
    localStorage.removeItem('token');
    this.setState({
      uid: null
    });
  }

  componentWillMount() {
    var token = localStorage.getItem('token');
    if(token) {
      ref.authWithCustomToken(token, this.authHandler);
    }
  }

  authHandler(err, authData) {
    if(err) {
      console.error(err);
      return;
    }

    // save the login token in the browser
    localStorage.setItem('token', authData.token);

    const storeRef = ref.child(this.props.params.storeId);

    storeRef.on('value', (snapshot)=> {
      var data = snapshot.val() || {};

      // claim if there is no owner
      if(!data.owner) {
        storeRef.set({
          owner: authData.uid
        });
      }

      // update state to reflect store owner
      this.setState({
        uid: authData.uid,
        owner: data.owner || authData.uid
      });
    });
  }

  renderLogin() {
    return (
      <nav className="login">
        <h2>Inventory</h2>
        <p>Sign in manage your store's inventory</p>
        <button className="github" onClick={this.authenticate.bind(this, 'github')}>Log in with Github</button>
      </nav>
    )
  }

  renderInventory(key) {
    var linkState = this.props.linkState;
    return (
      <div className="fish-edit" key={key}>
        <input type="text" valueLink={linkState(`fishes.${key}.name`)} />
        <input type="text" valueLink={linkState(`fishes.${key}.price`)} />

        <select valueLink={linkState(`fishes.${key}.status`)}>
          <option value="unavailable">Sold Out!</option>
          <option value="available">Fresh!</option>
        </select>
        <input type="text" valueLink={linkState(`fishes.${key}.image`)} />
        <button onClick={this.props.removeFish.bind(null, key)}>Remove Fish</button>
      </div>
    )
  }

  render() {

    let logoutButton = <button onClick={this.logout}>Log Out!</button>

    // check if a user is logged in
    if(!this.state.uid) {
      return (
        <div>{this.renderLogin()}</div>
      )
    }

    // check if current user isn't the owner
    if(this.state.uid !== this.state.owner) {
      return (
        <div>
          <p>Sorry, you aren't the store owner.</p>
          {logoutButton}
        </div>
      )
    }

    /*
    here 'this.props.fishes' gets its value from props which
    were assigned the fishes state in the parent component
    */
    return (
      <div>
        <h2>Inventory</h2>
        {logoutButton}
        {Object.keys(this.props.fishes).map(this.renderInventory)}

        <AddFishForm {...this.props} />
        <button onClick={this.props.loadSamples}>Load Sample Fishes</button>
      </div>
    )
  }
}

Inventory.propTypes= {
  addFish: React.PropTypes.func.isRequired,
  removeFish: React.PropTypes.func.isRequired,
  loadSamples: React.PropTypes.func.isRequired,
  fishes: React.PropTypes.object.isRequired,
  linkState: React.PropTypes.func.isRequired
}

export default Inventory;
