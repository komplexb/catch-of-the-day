/*
  StorePicker
  This will let us make <StorePicker/>
*/
import React from 'react';
import { History } from 'react-router'; //= var Navigation = ReactRouter.Navigation;
import h from '../helpers';
import reactMixin from 'react-mixin';
import autobind from 'autobind-decorator';

@autobind
class StorePicker extends React.Component {

  goToStore(e) {
    e.preventDefault();
    // get the data from the input
    var storeId = this.refs.storeId.value; // set by the ReactRouter.History
    this.history.pushState(null, `/store/${storeId}`);
  }

  render() {
    return (
      <form className="store-selector" onSubmit={this.goToStore}>
        <h2>Please Enter A Store</h2>
        <input type="text" ref="storeId" defaultValue={h.getFunName()} required />
        <input type="Submit" />
      </form>
    )
  }

}

reactMixin.onClass(StorePicker, History);

export default StorePicker;
