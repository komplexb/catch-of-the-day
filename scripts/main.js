import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route } from 'react-router';
import { createHistory } from 'history';
import Catalyst from 'react-catalyst'; // mixin
import reactMixin from 'react-mixin';
import autobind from 'autobind-decorator';

// Firebase
var Rebase = require('re-base');
var base = Rebase.createClass('https://sweltering-heat-8829.firebaseio.com/');

/*
 Import Components
*/
import NotFound from './components/NotFound';
import StorePicker from './components/StorePicker';
import Fish from './components/Fish';
import Inventory from './components/Inventory';
import Header from './components/Header';
import Order from './components/Order';

/*
  App
*/
@autobind
class App extends React.Component {

  constructor() {
    super();

    this.state = {
      fishes: {},
      order: {}
    }
  }

  /*
    Invoked once, only on the client (not on the server), immediately after the initial rendering occurs.
    If you want to integrate with other JavaScript frameworks, set timers using setTimeout or setInterval, or send AJAX requests, perform those operations in this method.
  */
  componentDidMount() {
    base.syncState(`${this.props.params.storeId}/fishes`, {
      context: this,
      state: 'fishes'
    });

    /*
      Rebase api will have promises soon,
      that way we can do this localStorage stuff after firebase indicates its
      complete
    */

    // order info is being stored and retrieved from localStorage

    var localStorageRef = localStorage.getItem(`order-${this.props.params.storeId}`);

    if(localStorageRef) {
      // update our component state to reflect what is in localStorage
      this.setState({
        order: JSON.parse(localStorageRef)
      });
    }
  }

  /*
    Sort of like an event listener for when the data will change - Wes Bos
    Use this as an opportunity to perform preparation before an update occurs.
  */
  componentWillUpdate(nextProps, nextState) {
    localStorage.setItem(`order-${this.props.params.storeId}`, JSON.stringify(nextState.order));
  }

  addToOrder(key) {
    this.state.order[key] =  this.state.order[key] + 1 || 1;
    this.setState({ order: this.state.order });
  }

  addFish(fish) {
    var timestamp = (new Date()).getTime();

    // update state object
    this.state.fishes['fish-' + timestamp] = fish;
    this.setState({ fishes: this.state.fishes });
  }

  removeFromOrder(key) {
    delete this.state.order[key];
    this.setState({ order: this.state.order });
  }

  removeFish(key) {
    if(confirm(`Are you sure you want to remove inventory item: "${this.state.fishes[key].name}"!?`)) {
      this.state.fishes[key] = null;
      this.setState({
        fishes: this.state.fishes
      });
    }
  }

  loadSamples() {
    this.setState({
      fishes: require('./sample-fishes')
    });
  }

  renderFish(key) {
    return (
      <Fish addToOrder={this.addToOrder} key={key} index={key}

      details={this.state.fishes[key]}></Fish>
    )
  }

  render() {
    return (
      <div className="catch-of-the-day">
        <div className="menu">
          <Header tagline="Fresh Seafood Market" />
          <ul className="list-of-fishes">
            {
              Object.keys(this.state.fishes).map(this.renderFish)
            }
          </ul>
        </div>
        <Order fishes={this.state.fishes} order={this.state.order} removeOrder={this.removeFromOrder} />
        <Inventory {...this.props} addFish={this.addFish} removeFish={this.removeFish} loadSamples={this.loadSamples}
          fishes={this.state.fishes} linkState={this.linkState.bind(this)} />
      </div>
    )
  }
};

/*
  <Inventory linkState={this.linkState.bind(this)} />
  Since linkState is a mixin it doesn't benefit from the @autobind
*/
reactMixin.onClass(App, Catalyst.LinkedStateMixin);


/*
  Routes
*/

var routes = (
  <Router history={createHistory()}>
    <Route path="/" component={StorePicker}/>
    <Route path="/store/:storeId" component={App}/>
    <Route path="*" component={NotFound}/>
  </Router>
)

ReactDOM.render(routes, document.querySelector('#main'));
